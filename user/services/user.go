package services

import (
	"gitlab.com/fajarachmadyusup13/mini-kumparan/user/repository"
	"gitlab.com/fajarachmadyusup13/mini-kumparan/user/repository/inmemory"
	"gitlab.com/fajarachmadyusup13/mini-kumparan/user/storage"
)

// UserService wrap article services
type UserService struct {
	Query repository.UserQuery
}

// NewUserService creates new article service instance
func NewUserService() *UserService {
	var userQuery repository.UserQuery

	db := storage.NewUserStorage()
	userQuery = inmemory.NewUserQueryInMemory(db)

	return &UserService{
		Query: userQuery,
	}
}
