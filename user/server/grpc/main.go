package main

import (
	"log"
	"net"

	"gitlab.com/fajarachmadyusup13/mini-kumparan/user/config"
	pb "gitlab.com/fajarachmadyusup13/mini-kumparan/user/proto"
	"gitlab.com/fajarachmadyusup13/mini-kumparan/user/server/grpc/handler"
	"gitlab.com/fajarachmadyusup13/mini-kumparan/user/services"
	"google.golang.org/grpc"
)

func main() {
	port := ":9001"
	srv := grpc.NewServer()
	var userServer handler.UserServer

	userServer.UserService = services.NewUserService()
	userServer.ArticleService = config.InitArticleGRPC()

	pb.RegisterUserServiceServer(srv, userServer)

	log.Println("Starting user RPC server at", port)

	listen, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("Could not listen to %s: %v", port, err)
	}

	log.Fatal("User server is listening", srv.Serve(listen))
}
