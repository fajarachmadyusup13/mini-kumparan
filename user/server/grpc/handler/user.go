package handler

import (
	"context"
	"errors"
	"log"

	"gitlab.com/fajarachmadyusup13/mini-kumparan/user/domain"
	pb "gitlab.com/fajarachmadyusup13/mini-kumparan/user/proto"
	"gitlab.com/fajarachmadyusup13/mini-kumparan/user/services"
)

// UserServer implement article grpc server
type UserServer struct {
	UserService    *services.UserService
	ArticleService pb.ArticleServiceClient
}

// FindUserByID get all articles
func (as UserServer) FindUserByID(ctx context.Context, param *pb.UserRequest) (*pb.User, error) {
	result := as.UserService.Query.FindUserByID(param.ID)

	if result.Error != nil {
		return &pb.User{}, result.Error
	}
	if result.Result == nil {
		return &pb.User{}, errors.New("No result")
	}

	user := result.Result.(domain.User)
	var userResponse pb.User

	userResponse.ID = user.ID.String()
	userResponse.Name = user.Name
	userResponse.Email = user.Email

	// Get article details from user
	userIDRequest := &pb.UserIDRequest{
		UserId: param.ID,
	}

	articles, err := as.ArticleService.FindArticlesByUserID(ctx, userIDRequest)
	if err != nil {
		log.Println(err)
	}

	// attach articles by user id into user response
	userResponse.Articles = articles

	return &userResponse, nil
}
