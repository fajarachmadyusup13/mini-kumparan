package storage

import (
	"log"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/fajarachmadyusup13/mini-kumparan/user/domain"
)

// UserStorage wrap inmemory storage
type UserStorage struct {
	StorageMap []domain.User
}

// NewUserStorage create new article storage instance
func NewUserStorage() *UserStorage {
	// userID1 := uuid.NewV4()
	userID1, _ := uuid.FromString("815b95be-4741-4167-8224-975de6645de9")
	userID2, _ := uuid.FromString("42217852-9bb2-44f8-baea-fd5dbdced2b7")

	users := []domain.User{
		domain.User{
			ID:    userID1,
			Name:  "Moo",
			Email: "moo@example.com",
		},
		domain.User{
			ID:    userID2,
			Name:  "Poo",
			Email: "poo@example.com",
		},
	}

	log.Println("Preserved user ID for testing : ", userID1)
	log.Println("Preserved user ID for testing : ", userID2)

	return &UserStorage{
		StorageMap: users,
	}
}
