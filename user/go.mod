module gitlab.com/fajarachmadyusup13/mini-kumparan/user

go 1.12

require (
	github.com/golang/protobuf v1.3.2
	github.com/satori/go.uuid v1.2.0
	github.com/stretchr/testify v1.3.0
	gitlab.com/fajarachmadyusup13/mini-kumparan/article v0.0.0-20190727054221-46065bf5090f
	google.golang.org/grpc v1.22.1
)
