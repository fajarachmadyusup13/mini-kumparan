package domain

import (
	uuid "github.com/satori/go.uuid"
)

// User model
type User struct {
	ID    uuid.UUID `json:"id"`
	Name  string    `json:"name"`
	Email string    `json:"email"`
}
