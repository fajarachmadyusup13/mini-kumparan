package inmemory

import (
	"testing"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/fajarachmadyusup13/mini-kumparan/user/domain"
	"gitlab.com/fajarachmadyusup13/mini-kumparan/user/storage"
)

func TestCanFindUserByID(t *testing.T) {
	t.Run("Correct", func(t *testing.T) {

		// GIVEN
		userID1 := uuid.NewV4()
		userID2 := uuid.NewV4()

		users := []domain.User{
			domain.User{
				ID:    userID1,
				Name:  "Moo",
				Email: "moo@example.com",
			},
			domain.User{
				ID:    userID2,
				Name:  "Poo",
				Email: "poo@example.com",
			},
		}

		storage := storage.UserStorage{
			StorageMap: users,
		}

		query := NewUserQueryInMemory(&storage)

		// WHEN
		user := query.FindUserByID(userID1.String())

		// THEN
		assert.Equal(t, users[0], user.Result.(domain.User))
		assert.NoError(t, user.Error)

	})

	t.Run("Failed", func(t *testing.T) {
		// GIVEN
		userID1 := uuid.NewV4()
		userID2 := uuid.NewV4()

		users := []domain.User{
			domain.User{
				ID:    userID1,
				Name:  "Moo",
				Email: "moo@example.com",
			},
		}

		storage := storage.UserStorage{
			StorageMap: users,
		}

		query := NewUserQueryInMemory(&storage)

		// WHEN
		user := query.FindUserByID(userID2.String())

		// THEN
		assert.NotEmpty(t, user.Error)

	})
}
