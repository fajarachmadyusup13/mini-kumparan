package inmemory

import (
	"errors"

	"gitlab.com/fajarachmadyusup13/mini-kumparan/user/repository"
	"gitlab.com/fajarachmadyusup13/mini-kumparan/user/storage"
)

// UserQueryInMemory implements user query interface
type UserQueryInMemory struct {
	Storage *storage.UserStorage
}

// NewUserQueryInMemory create UserQueryInMemory instance
func NewUserQueryInMemory(storage *storage.UserStorage) repository.UserQuery {
	return UserQueryInMemory{
		Storage: storage,
	}
}

// FindUserByID implements FindUserByID from user query
func (uq UserQueryInMemory) FindUserByID(id string) repository.QueryResult {
	var response repository.QueryResult
	for _, user := range uq.Storage.StorageMap {
		if user.ID.String() == id {
			response.Result = user
			return response
		}
	}

	return repository.QueryResult{
		Error: errors.New("User not found"),
	}
}
