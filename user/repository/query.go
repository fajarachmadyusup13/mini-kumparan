package repository

// QueryResult store query result
type QueryResult struct {
	Result interface{}
	Error  error
}

// UserQuery wrap user query
type UserQuery interface {
	FindUserByID(id string) QueryResult
}
