test-article:
	@cd article && go test ./... -v -coverprofile=article_coverage.out 
	@cd article && go tool cover -html=article_coverage.out

test-user:
	@cd user && go test -v ./... -cover

run-article-server:
	@cd article && go run ./server/grpc/main.go

run-user-server:
	@cd user && go run ./server/grpc/main.go

run-graphql-server:
	@cd graphql && go run ./main.go

generate-proto:
	@sh ./proto-generator.sh

docker-prep: generate-proto
	@cd article/server/grpc/ && GOOS=linux go build . && echo "\033[0;32m[*] article server built \033[0m"
	@cd user/server/grpc/ && GOOS=linux go build . && echo "\033[0;32m[*] user server built \033[0m"
	@cd graphql/ && GOOS=linux go build . && echo "\033[0;32m[*] graphql server built \033[0m"
