package resolver

import (
	"context"

	"gitlab.com/fajarachmadyusup13/mini-kumparan/graphql/domain"
)

// ArticleResolver is to resolve article
type ArticleResolver struct {
	m domain.Article
	a domain.Author
}

// ID to resolve id
func (r *ArticleResolver) ID(ctx context.Context) *string {
	result := r.m.ID.String()
	return &result
}

// Name is to resolve
func (r *ArticleResolver) Name(ctx context.Context) *string {
	return &r.m.Name
}

// Slug is to resolve slug name
func (r *ArticleResolver) Slug(ctx context.Context) *string {
	return &r.m.Slug
}

// Body is to resolve body
func (r *ArticleResolver) Body(ctx context.Context) *string {
	return &r.m.Body
}

// CreatedAt is to  created
func (r *ArticleResolver) CreatedAt(ctx context.Context) *string {
	result := r.m.CreatedAt.String()
	return &result
}

// UpdatedAt is
func (r *ArticleResolver) UpdatedAt(ctx context.Context) *string {
	result := r.m.UpdatedAt.String()
	return &result
}

func (r *ArticleResolver) User(ctx context.Context) *AuthorResolver {
	result := &AuthorResolver{
		m: r.a,
	}
	return result
}
