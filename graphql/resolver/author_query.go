package resolver

import (
	"context"
	"log"

	"gitlab.com/fajarachmadyusup13/mini-kumparan/graphql/domain"
	pb "gitlab.com/fajarachmadyusup13/mini-kumparan/graphql/proto"
)

// FindUserByID find user by user id
func (r *Resolver) FindUserByID(ctx context.Context, args struct{ ID string }) (*AuthorResolver, error) {
	var result *AuthorResolver

	userRequest := pb.UserRequest{
		ID: args.ID,
	}

	author, err := r.AuthorService.FindUserByID(ctx, &userRequest)
	if err != nil && err.Error() != "No result" {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	userIDRequest := pb.UserIDRequest{
		UserId: args.ID,
	}

	articles, err := r.ArticleService.FindArticlesByUserID(ctx, &userIDRequest)
	if err != nil && err.Error() != "Not found" {
		log.Println(err)
	}

	articleResult := make([]domain.Article, len(articles.ArticleList))
	for v := range articles.ArticleList {
		articleResult[v] = domain.ConvertFromProtoToArticle(articles.ArticleList[v])
	}

	result = &AuthorResolver{
		m:  domain.ConvertFromProtoToAuthor(author),
		ar: articleResult,
	}

	return result, err
}
