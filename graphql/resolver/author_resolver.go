package resolver

import (
	"context"

	"gitlab.com/fajarachmadyusup13/mini-kumparan/graphql/domain"
)

type AuthorResolver struct {
	m  domain.Author
	ar []domain.Article
}

func (r *AuthorResolver) ID(ctx context.Context) *string {
	result := r.m.ID.String()
	return &result
}

func (r *AuthorResolver) Name(ctx context.Context) *string {
	result := r.m.Name
	return &result
}

func (r *AuthorResolver) Email(ctx context.Context) *string {
	result := r.m.Email
	return &result
}

func (r *AuthorResolver) Articles(ctx context.Context) *[]*ArticleResolver {
	var articles = make([]*ArticleResolver, len(r.ar))

	for i, v := range r.ar {
		articles[i] = &ArticleResolver{
			m: v,
		}
	}

	return &articles
}
