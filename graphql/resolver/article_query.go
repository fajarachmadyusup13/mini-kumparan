package resolver

import (
	"context"
	"gitlab.com/fajarachmadyusup13/mini-kumparan/graphql/domain"
	"log"

	"github.com/golang/protobuf/ptypes/empty"
	pb "gitlab.com/fajarachmadyusup13/mini-kumparan/graphql/proto"
)

// GetArticles is to get articles
func (r *Resolver) GetArticles(ctx context.Context) (*[]*ArticleResolver, error) {
	// Ngobrol ke grpc
	// ------------------------------------------
	articles, err := r.ArticleService.GetAllArticles(ctx, &empty.Empty{})
	if err != nil {
		return nil, err
	}

	result := make([]*ArticleResolver, len(articles.ArticleList))
	for v, value := range articles.ArticleList {

		userRequest := &pb.UserRequest{
			ID: value.UserId,
		}

		author, err := r.AuthorService.FindUserByID(ctx, userRequest)
		if err != nil {
			log.Println(err)
		}

		result[v] = &ArticleResolver{
			m: domain.ConvertFromProtoToArticle(articles.ArticleList[v]),
			a: domain.ConvertFromProtoToAuthor(author),
		}
	}
	return &result, err
}

// // FindArticleByID is to find article by id
// func (r *Resolver) FindArticleByID(ctx context.Context, args struct{ ID string }) (*ArticleResolver, error) {
// 	article, err := r.ArticleService.FindArticleByID(args.ID)

// 	if err != nil {
// 		return nil, errors.Wrap(err, "FindArticleByID")
// 	}

// 	result := ArticleResolver{m: article}
// 	return &result, nil
// }
