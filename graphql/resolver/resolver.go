package resolver

import (
	pb "gitlab.com/fajarachmadyusup13/mini-kumparan/graphql/proto"
)

// Resolver is to resolve
type Resolver struct {
	// Ganti koneksi ke grpc
	// ------------------------------------------
	ArticleService pb.ArticleServiceClient
	AuthorService  pb.UserServiceClient
}
