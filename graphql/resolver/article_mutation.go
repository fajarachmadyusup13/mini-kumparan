package resolver

import (
	"context"

	"gitlab.com/fajarachmadyusup13/mini-kumparan/graphql/domain"
	pb "gitlab.com/fajarachmadyusup13/mini-kumparan/graphql/proto"
)

// ArticleInput input result
type ArticleInput struct {
	Name   string
	Body   string
	Slug   string
	UserID string
}

// CreateArticle to create
func (r *Resolver) CreateArticle(ctx context.Context, args struct{ Article ArticleInput }) (*ArticleResolver, error) {
	articleRequest := &pb.ArticleRequest{
		Name:   args.Article.Name,
		Body:   args.Article.Body,
		Slug:   args.Article.Slug,
		UserId: args.Article.UserID,
	}

	result, err := r.ArticleService.CreateArticle(ctx, articleRequest)
	if err != nil {
		return nil, err
	}

	convertedResult := domain.ConvertFromProtoToArticle(result)

	resolver := ArticleResolver{m: convertedResult}

	return &resolver, nil
}
