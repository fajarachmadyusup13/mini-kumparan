package services

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/fajarachmadyusup13/mini-kumparan/graphql/domain"
	"gitlab.com/fajarachmadyusup13/mini-kumparan/graphql/repository"
	"gitlab.com/fajarachmadyusup13/mini-kumparan/graphql/repository/inmemory"
	"gitlab.com/fajarachmadyusup13/mini-kumparan/graphql/storage"
)

type AuthorService struct {
	Query repository.AuthorQuery
}

func NewAuthorService() *AuthorService {
	var query repository.AuthorQuery

	db := storage.NewAuthorStorage()
	query = inmemory.NewAuthorQueryInMemory(db)

	return &AuthorService{
		Query: query,
	}
}

func (service *AuthorService) FindAuthorByID(uid uuid.UUID) (domain.Author, error) {
	result := <-service.Query.FindAuthorByID(uid)
	if result.Error != nil {
		return domain.Author{}, result.Error
	}

	if result.Result == nil {
		return domain.Author{}, result.Error
	}

	return result.Result.(domain.Author), nil
}
