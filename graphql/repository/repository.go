package repository

import "gitlab.com/fajarachmadyusup13/mini-kumparan/graphql/domain"

// ArticleRepository is wrap contract article repository
type ArticleRepository interface {
	Save(article *domain.Article) <-chan error
}

// PageRepository is wrap contract article repository
type PageRepository interface {
	Save(name string) <-chan error
}
