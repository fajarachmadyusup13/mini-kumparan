package inmemory

import (
	"gitlab.com/fajarachmadyusup13/mini-kumparan/graphql/domain"
	"gitlab.com/fajarachmadyusup13/mini-kumparan/graphql/repository"
	"gitlab.com/fajarachmadyusup13/mini-kumparan/graphql/storage"
)

// ArticleRepositoryInMemory is article repository implementation in memory
type ArticleRepositoryInMemory struct {
	Storage *storage.ArticleStorage
}

// NewArticleRepositoryInMemory is to create ArticleRepositoryInMemory instance
func NewArticleRepositoryInMemory(storage *storage.ArticleStorage) repository.ArticleRepository {
	return &ArticleRepositoryInMemory{Storage: storage}
}

// Save is for save article
func (repo *ArticleRepositoryInMemory) Save(article *domain.Article) <-chan error {
	result := make(chan error)

	go func() {
		repo.Storage.ArticleMap = append(repo.Storage.ArticleMap, *article)

		result <- nil
		close(result)
	}()

	return result
}
