package inmemory

import (
	"errors"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/fajarachmadyusup13/mini-kumparan/graphql/domain"
	"gitlab.com/fajarachmadyusup13/mini-kumparan/graphql/repository"
	"gitlab.com/fajarachmadyusup13/mini-kumparan/graphql/storage"
)

type AuthorQueryInMemory struct {
	Storage *storage.AuthorStorage
}

// NewArticleQueryInMemory is to Create Instance ArticleQueryInMemory
func NewAuthorQueryInMemory(storage *storage.AuthorStorage) repository.AuthorQuery {
	return &AuthorQueryInMemory{Storage: storage}
}

func (query *AuthorQueryInMemory) FindAuthorByID(id uuid.UUID) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)

	go func() {
		author := domain.Author{}
		for _, v := range query.Storage.AuthorMap {
			if v.ID == id {
				author = v
			}
		}
		if author.Name == "" {
			result <- repository.QueryResult{Error: errors.New("Author not found")}
		} else {
			result <- repository.QueryResult{Result: author}
		}

		close(result)

	}()

	return result
}
