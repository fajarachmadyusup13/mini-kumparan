package domain

import (
	uuid "github.com/satori/go.uuid"
	pb "gitlab.com/fajarachmadyusup13/mini-kumparan/graphql/proto"
)

// Author wrapper
type Author struct {
	ID    uuid.UUID
	Name  string
	Email string
}

// CreateAuthor is to create author
func CreateAuthor(name, email string) (*Author, error) {
	uid, err := uuid.NewV4()
	if err != nil {
		return &Author{}, err
	}

	return &Author{
		ID:    uid,
		Name:  name,
		Email: email,
	}, nil
}

// ConvertFromProtoToAuthor convert from user proto struct into domain author struct
func ConvertFromProtoToAuthor(param *pb.User) (author Author) {
	author.ID, _ = uuid.FromString(param.ID)
	author.Name = param.Name
	author.Email = param.Email
	return
}
