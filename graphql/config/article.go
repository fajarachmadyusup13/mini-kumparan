package config

import (
	"log"

	pb "gitlab.com/fajarachmadyusup13/mini-kumparan/graphql/proto"
	"google.golang.org/grpc"
)

// InitArticleGRPC init the article GRPC client
func InitArticleGRPC() pb.ArticleServiceClient {
	host := "article:9002"

	conn, err := grpc.Dial(host, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Could not connect to %s: %v", host, err)
	}

	return pb.NewArticleServiceClient(conn)
}
