package storage

import (
	"log"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/fajarachmadyusup13/mini-kumparan/graphql/domain"
)

// ArticleStorage is article storage in memory
type ArticleStorage struct {
	ArticleMap []domain.Article
}

// NewArticleStorage is to create article storage
func NewArticleStorage() *ArticleStorage {
	article1, _ := domain.CreateArticle("Pelita Harapan", "Ini pelita harapan body")
	article2, _ := domain.CreateArticle("Jantung Harapan", "Ini jantung harapan body")

	userID, err := uuid.FromString("df1f4a9a-0d06-409f-91dc-053d7f93f90f")
	if err != nil {
		log.Println(err)
	}

	err = article1.AttachUserID(userID)
	if err != nil {
		log.Println(err)
	}

	userID, err = uuid.FromString("0c07bbb0-3fa3-4ca7-925e-c9aa77320bba")
	if err != nil {
		log.Println(err)
	}

	err = article2.AttachUserID(userID)
	if err != nil {
		log.Println(err)
	}

	return &ArticleStorage{
		ArticleMap: []domain.Article{
			*article1, *article2,
		},
	}
}

// AuthorStorage is to create author
type AuthorStorage struct {
	AuthorMap []domain.Author
}

// NewAuthorStorage s
func NewAuthorStorage() *AuthorStorage {
	userID1, _ := uuid.FromString("df1f4a9a-0d06-409f-91dc-053d7f93f90f")
	userID2, _ := uuid.FromString("0c07bbb0-3fa3-4ca7-925e-c9aa77320bba")

	author1 := domain.Author{
		ID:    userID1,
		Name:  "Fajar",
		Email: "fajar@example.com",
	}
	author2 := domain.Author{
		ID:    userID2,
		Name:  "Hasbi",
		Email: "hasbi@example.com",
	}

	return &AuthorStorage{
		AuthorMap: []domain.Author{author1, author2},
	}
}
