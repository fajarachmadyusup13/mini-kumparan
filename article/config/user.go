package config

import (
	"log"

	pb "gitlab.com/fajarachmadyusup13/mini-kumparan/article/proto"
	"google.golang.org/grpc"
)

// InitUserGRPC init a user GRPC client
func InitUserGRPC() pb.UserServiceClient {
	host := "user:9001"

	conn, err := grpc.Dial(host, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Could not connect to %s: %v", host, err)
	}

	return pb.NewUserServiceClient(conn)
}
