package inmemory

import (
	"gitlab.com/fajarachmadyusup13/mini-kumparan/article/domain"
	"gitlab.com/fajarachmadyusup13/mini-kumparan/article/repository"
	"gitlab.com/fajarachmadyusup13/mini-kumparan/article/storage"
)

// ArticleRepositoryInMemory store article repository inmemory
type ArticleRepositoryInMemory struct {
	Storage *storage.ArticleStorage
}

// NewArticleRepositoryInMemory create new article repository
func NewArticleRepositoryInMemory(storage *storage.ArticleStorage) repository.ArticleRepository {
	return ArticleRepositoryInMemory{
		Storage: storage,
	}
}

// SaveArticle save article to storage
func (ar ArticleRepositoryInMemory) SaveArticle(article *domain.Article) error {
	ar.Storage.ArticleMap = append(ar.Storage.ArticleMap, *article)
	return nil
}
