package inmemory

import (
	"errors"

	"gitlab.com/fajarachmadyusup13/mini-kumparan/article/domain"
	"gitlab.com/fajarachmadyusup13/mini-kumparan/article/repository"
	"gitlab.com/fajarachmadyusup13/mini-kumparan/article/storage"
)

// ArticleQueryInMemory store article query inmemory
type ArticleQueryInMemory struct {
	Storage *storage.ArticleStorage
}

// NewArticleQueryInMemory create new article query inmemory instance
func NewArticleQueryInMemory(storage *storage.ArticleStorage) repository.ArticleQuery {
	return ArticleQueryInMemory{
		Storage: storage,
	}
}

// GetAllArticles get all articles
func (aq ArticleQueryInMemory) GetAllArticles() repository.QueryResult {
	return repository.QueryResult{
		Result: aq.Storage.ArticleMap,
	}
}

// FindArticlesByUserID find articles by id
func (aq ArticleQueryInMemory) FindArticlesByUserID(id string) repository.QueryResult {
	var result []domain.Article

	for _, article := range aq.Storage.ArticleMap {
		if article.UserID.String() == id {
			result = append(result, article)
		}
	}

	if len(result) == 0 {
		return repository.QueryResult{Error: errors.New("Not found")}
	}

	return repository.QueryResult{Result: result}
}
