package inmemory

import (
	"testing"

	"gitlab.com/fajarachmadyusup13/mini-kumparan/article/domain"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/fajarachmadyusup13/mini-kumparan/article/storage"
)

func TestCanCreateArticleQuery(t *testing.T) {
	t.Run("Correct GetAllArticles", func(t *testing.T) {
		// given
		articleID1 := uuid.NewV4()
		articleID2 := uuid.NewV4()
		userID1 := uuid.NewV4()
		userID2 := uuid.NewV4()

		article1 := domain.Article{
			ID:     articleID1,
			Slug:   "artikel-lama",
			Name:   "Artikel Lama",
			Body:   "Ini body artikel lama",
			UserID: userID1,
		}
		article2 := domain.Article{
			ID:     articleID2,
			Slug:   "artikel-lama",
			Name:   "Artikel Lama",
			Body:   "Ini body artikel lama",
			UserID: userID2,
		}

		storage := storage.ArticleStorage{
			ArticleMap: []domain.Article{
				article1,
				article2,
			},
		}

		query := NewArticleQueryInMemory(&storage)

		// when
		articles := query.GetAllArticles()

		// them
		assert.NotEmpty(t, storage.ArticleMap)
		assert.NoError(t, articles.Error)
	})

	t.Run("Correct FindArticlesByUser", func(t *testing.T) {
		// given
		articleID1 := uuid.NewV4()
		articleID2 := uuid.NewV4()
		articleID3 := uuid.NewV4()

		userID1 := uuid.NewV4()
		userID2 := uuid.NewV4()

		article1 := domain.Article{
			ID:     articleID1,
			Slug:   "artikel-lama",
			Name:   "Artikel Lama",
			Body:   "Ini body artikel lama",
			UserID: userID1,
		}
		article2 := domain.Article{
			ID:     articleID2,
			Slug:   "artikel-lama",
			Name:   "Artikel Lama",
			Body:   "Ini body artikel lama",
			UserID: userID2,
		}
		article3 := domain.Article{
			ID:     articleID3,
			Slug:   "artikel-kemaren",
			Name:   "Artikel Kemaren",
			Body:   "Ini body artikel kemaren",
			UserID: userID1,
		}

		storage := storage.ArticleStorage{
			ArticleMap: []domain.Article{
				article1,
				article2,
				article3,
			},
		}

		query := NewArticleQueryInMemory(&storage)

		// when
		articles := query.FindArticlesByUserID(userID1.String())

		// them
		assert.NotEmpty(t, storage.ArticleMap)
		assert.NoError(t, articles.Error)
	})

	t.Run("No Data GetAllArticles", func(t *testing.T) {
		// given
		storage := storage.ArticleStorage{
			ArticleMap: []domain.Article{},
		}

		query := NewArticleQueryInMemory(&storage)

		// when
		articles := query.FindArticlesByUserID("123")

		// them
		assert.Empty(t, storage.ArticleMap)
		assert.Error(t, articles.Error)
	})
}
