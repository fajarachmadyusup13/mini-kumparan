package inmemory

import (
	"testing"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/fajarachmadyusup13/mini-kumparan/article/domain"
	"gitlab.com/fajarachmadyusup13/mini-kumparan/article/storage"
)

func TestCanSaveArticle(t *testing.T) {
	t.Run("", func(t *testing.T) {
		// given
		articleID1 := uuid.NewV4()
		articleID2 := uuid.NewV4()
		articleID3 := uuid.NewV4()
		userID1 := uuid.NewV4()
		userID2 := uuid.NewV4()

		article1 := domain.Article{
			ID:     articleID1,
			Slug:   "artikel-lama",
			Name:   "Artikel Lama",
			Body:   "Ini body artikel lama",
			UserID: userID1,
		}
		article2 := domain.Article{
			ID:     articleID2,
			Slug:   "artikel-lama",
			Name:   "Artikel Lama",
			Body:   "Ini body artikel lama",
			UserID: userID2,
		}
		article3 := domain.Article{
			ID:     articleID3,
			Slug:   "artikel-besok",
			Name:   "Artikel Besok",
			Body:   "Ini body artikel Besok",
			UserID: userID1,
		}

		storage := storage.ArticleStorage{
			ArticleMap: []domain.Article{
				article1,
				article2,
			},
		}

		repo := NewArticleRepositoryInMemory(&storage)

		// when
		err := repo.SaveArticle(&article3)

		// then
		assert.NoError(t, err)
		assert.Contains(t, storage.ArticleMap, article3)
	})
}
