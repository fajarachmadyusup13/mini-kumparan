package repository

// QueryResult store a query result
type QueryResult struct {
	Result interface{}
	Error  error
}

type ArticleQuery interface {
	GetAllArticles() QueryResult
	FindArticlesByUserID(id string) QueryResult
}
