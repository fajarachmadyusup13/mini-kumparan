package repository

import (
	"gitlab.com/fajarachmadyusup13/mini-kumparan/article/domain"
)

// ArticleRepository wrap repository article interface
type ArticleRepository interface {
	SaveArticle(*domain.Article) error
}
