package domain

import (
	"time"

	uuid "github.com/satori/go.uuid"
	pb "gitlab.com/fajarachmadyusup13/mini-kumparan/article/proto"
)

const (
	PUBLISHED string = "PUBLISHED"
	DRAFT     string = "DRAFT"
)

// Article store a single article
type Article struct {
	ID        uuid.UUID `json:"id"`
	Slug      string    `json:"slug"`
	Name      string    `json:"name"`
	Body      string    `json:"body"`
	Status    string    `json:"status"`
	UserID    uuid.UUID `json:"user_id"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
	User      *pb.User  `json:"user"`
}
