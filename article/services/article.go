package services

import (
	"gitlab.com/fajarachmadyusup13/mini-kumparan/article/repository"
	"gitlab.com/fajarachmadyusup13/mini-kumparan/article/repository/inmemory"
	"gitlab.com/fajarachmadyusup13/mini-kumparan/article/storage"
)

// ArticleService wrap article services
type ArticleService struct {
	Query      repository.ArticleQuery
	Repository repository.ArticleRepository
}

// NewArticleService creates new article service instance
func NewArticleService() *ArticleService {
	var articleQuery repository.ArticleQuery

	db := storage.NewArticleStorage()
	articleRepository := inmemory.NewArticleRepositoryInMemory(db)
	articleQuery = inmemory.NewArticleQueryInMemory(db)

	return &ArticleService{
		Query:      articleQuery,
		Repository: articleRepository,
	}
}
