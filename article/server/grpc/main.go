package main

import (
	"log"
	"net"

	"gitlab.com/fajarachmadyusup13/mini-kumparan/article/config"
	pb "gitlab.com/fajarachmadyusup13/mini-kumparan/article/proto"
	"gitlab.com/fajarachmadyusup13/mini-kumparan/article/server/grpc/handler"
	"gitlab.com/fajarachmadyusup13/mini-kumparan/article/services"
	"google.golang.org/grpc"
)

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
}

func main() {
	port := ":9002"
	srv := grpc.NewServer()
	var articleServer handler.ArticleServer

	articleServer.ArticleService = services.NewArticleService()
	articleServer.UserService = config.InitUserGRPC()

	pb.RegisterArticleServiceServer(srv, articleServer)

	log.Println("Starting article RPC server at", port)

	listen, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("Could not listen to %s: %v", port, err)
	}

	log.Fatal("Article server is listening", srv.Serve(listen))
}
