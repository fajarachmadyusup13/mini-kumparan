package handler

import (
	"context"
	"errors"
	"fmt"
	"log"
	"strings"
	"time"

	uuid "github.com/satori/go.uuid"

	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.com/fajarachmadyusup13/mini-kumparan/article/domain"
	pb "gitlab.com/fajarachmadyusup13/mini-kumparan/article/proto"
	"gitlab.com/fajarachmadyusup13/mini-kumparan/article/services"
)

// ArticleServer implement article grpc server
type ArticleServer struct {
	ArticleService *services.ArticleService
	UserService    pb.UserServiceClient
}

// GetAllArticles get all articles
func (as ArticleServer) GetAllArticles(ctx context.Context, _ *empty.Empty) (*pb.ArticleList, error) {
	result := as.ArticleService.Query.GetAllArticles()

	if result.Error != nil {
		return &pb.ArticleList{}, result.Error
	}
	if result.Result == nil {
		return &pb.ArticleList{}, errors.New("No result")
	}
	articles := result.Result.([]domain.Article)
	var articleResponse []*pb.Article
	for _, v := range articles {
		var article pb.Article

		article.ID = v.ID.String()
		article.Name = v.Name
		article.Slug = v.Slug
		article.Body = v.Body
		article.CreatedAt = v.CreatedAt.Format(time.RFC1123Z)
		article.UpdatedAt = v.UpdatedAt.Format(time.RFC1123Z)
		article.Status = v.Status
		article.UserId = v.UserID.String()

		// Get user detail via GRPC
		userRequest := &pb.UserRequest{
			ID: v.UserID.String(),
		}

		userDetail, err := as.UserService.FindUserByID(ctx, userRequest)
		if err != nil {
			log.Println(err)
		}

		// attach to temp article var
		article.User = userDetail

		articleResponse = append(articleResponse, &article)
	}

	return &pb.ArticleList{
		ArticleList: articleResponse,
	}, nil
}

// FindArticlesByUserID find articles by id
func (as ArticleServer) FindArticlesByUserID(ctx context.Context, param *pb.UserIDRequest) (*pb.ArticleList, error) {
	result := as.ArticleService.Query.FindArticlesByUserID(param.UserId)

	if result.Error != nil {
		return &pb.ArticleList{}, result.Error
	}
	if result.Result == nil {
		return &pb.ArticleList{}, errors.New("No Result")
	}

	articles := result.Result.([]domain.Article)
	var articleResponse []*pb.Article
	for _, v := range articles {
		var article pb.Article

		article.ID = v.ID.String()
		article.Name = v.Name
		article.Slug = v.Slug
		article.Body = v.Body
		article.CreatedAt = v.CreatedAt.String()
		article.UpdatedAt = v.UpdatedAt.String()
		article.Status = v.Status
		article.UserId = v.UserID.String()

		articleResponse = append(articleResponse, &article)
	}

	return &pb.ArticleList{
		ArticleList: articleResponse,
	}, nil
}

// CreateArticle create new article
func (as ArticleServer) CreateArticle(ctx context.Context, param *pb.ArticleRequest) (*pb.Article, error) {
	var article domain.Article

	if len(param.Slug) == 0 {
		temp := strings.Fields(param.Name)
		slug := strings.Join(temp, "-")
		timeStamp := time.Now().UnixNano()
		timeString := fmt.Sprintf("%d", timeStamp)
		slug += "-" + timeString

		article.Slug = slug
	}

	article.ID = uuid.NewV4()
	article.Name = param.Name
	article.Body = param.Body
	article.UserID, _ = uuid.FromString(param.UserId)
	article.Status = domain.DRAFT
	article.CreatedAt = time.Now()
	article.UpdatedAt = time.Now()

	err := as.ArticleService.Repository.SaveArticle(&article)
	if err != nil {
		return &pb.Article{}, err
	}

	return &pb.Article{
		ID:        article.ID.String(),
		Name:      article.Name,
		Body:      article.Body,
		Slug:      article.Slug,
		UserId:    article.UserID.String(),
		CreatedAt: article.CreatedAt.String(),
		UpdatedAt: article.UpdatedAt.String(),
	}, nil

}
