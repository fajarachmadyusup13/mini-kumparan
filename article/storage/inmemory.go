package storage

import (
	"time"

	"gitlab.com/fajarachmadyusup13/mini-kumparan/article/domain"

	uuid "github.com/satori/go.uuid"
)

// ArticleStorage wrap inmemory storage
type ArticleStorage struct {
	ArticleMap []domain.Article
}

// NewArticleStorage create new article storage instance
func NewArticleStorage() *ArticleStorage {

	articleID1 := uuid.NewV4()
	articleID2 := uuid.NewV4()
	articleID3 := uuid.NewV4()

	userID1, _ := uuid.FromString("815b95be-4741-4167-8224-975de6645de9")
	userID2, _ := uuid.FromString("42217852-9bb2-44f8-baea-fd5dbdced2b7")

	articles := []domain.Article{
		domain.Article{
			ID:        articleID1,
			Slug:      "artikel-lama",
			Name:      "Artikel Lama",
			Body:      "Ini body artikel lama",
			Status:    domain.DRAFT,
			UserID:    userID1,
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
		},
		domain.Article{
			ID:        articleID2,
			Slug:      "artikel-baru",
			Name:      "Artikel Baru",
			Body:      "Ini body artikel Lama",
			Status:    domain.PUBLISHED,
			UserID:    userID2,
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
		},
		domain.Article{
			ID:        articleID3,
			Slug:      "artikel-kemaren",
			Name:      "Artikel kemaren",
			Body:      "Ini body artikel kemaren",
			UserID:    userID1,
			Status:    domain.PUBLISHED,
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
		},
	}

	return &ArticleStorage{
		ArticleMap: articles,
	}
}
