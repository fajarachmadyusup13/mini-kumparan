function printSuccess() {
  echo "\033[0;32m[*] $1 proto successfully generated\033[0m"
}

protoc --go_out=plugins=grpc:. --proto_path=./article/proto ./article/proto/*.proto && printSuccess "article"
protoc --go_out=plugins=grpc:. --proto_path=./user/proto ./user/proto/*.proto && printSuccess "user"
protoc --go_out=plugins=grpc:. --proto_path=./graphql/proto ./graphql/proto/*.proto && printSuccess "graphql"